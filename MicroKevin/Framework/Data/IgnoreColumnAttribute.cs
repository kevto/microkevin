﻿namespace MicroKevin.Framework.Data
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// IgnoreColumnAttribute. 
    /// </summary>
    [AttributeUsage(AttributeTargets.Property, Inherited = true)]
    public class IgnoreColumnAttribute : Attribute
    {
        /// <summary>
        /// Ignore column
        /// </summary>
        private bool _ignore;

        /// <summary>
        /// Public property to get the ignore state.
        /// </summary>
        public bool ignore
        {
            get
            {
                return this._ignore;
            }
        }

        /// <summary>
        /// Public constructor. Sets the ignore state
        /// </summary>
        /// <param name="ignore">Ignore state</param>
        public IgnoreColumnAttribute(bool ignore)
        {
            this._ignore = ignore;
        }

        /// <summary>
        /// Public constructor.
        /// </summary>
        public IgnoreColumnAttribute()
        {
            this._ignore = true;
        }

        /// <summary>
        /// ToString override to get the ignore state name.
        /// </summary>
        /// <returns>Table name in a string</returns>
        public override string ToString()
        {
            return (this.ignore ? "true" : "false");
        }          
    }
}