﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using Oracle.ManagedDataAccess.Client;

namespace MicroKevin.Framework
{
    /// <summary>
    /// Database class manager.
    /// </summary>
    public class DbConnection
    {
        /// <summary>
        /// Get an open connection
        /// </summary>
        /// <param name="mars">Multiple Active Result Sets</param>
        /// <returns>Open SqlConnection instance</returns>
        public static OracleConnection GetOpenConnection(bool mars = false)
        {
            try
            {
                var cs = System.Configuration.ConfigurationManager.ConnectionStrings["OracleDbContext"].ConnectionString;
                var connection = new OracleConnection(cs);
                connection.Open();
                return connection;
            }
            catch (Exception ex)
            {
                Debug.WriteLine("MicroKevin: " + ex.Message);
            }

            return null;
        }

        /// <summary>
        /// Getting a closed connection
        /// </summary>
        /// <returns>Closed SqlConnection instance</returns>
        public static SqlConnection GetClosedConnection()
        {
            return new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["OracleDbContext"].ConnectionString);
        }
    }
}
