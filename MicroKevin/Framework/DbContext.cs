﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MicroKevin;
using System.Data;
using Oracle.ManagedDataAccess.Client;
using MicroKevin.Framework.Data;
using System.Reflection;
using MicroKevin.Framework.Data.Models;

namespace MicroKevin.Framework
{
    /// <summary>
    /// Context class of the framework
    /// </summary>
    public class DbContext : IDisposable
    {
        private static IDbConnection connection = null;

        /// <summary>
        /// Public constructor
        /// </summary>
        private DbContext()
        {
        }

        public static DbContext GetInstance()
        {
            if (connection == null)
                connection = DbConnection.GetOpenConnection();
            else if (connection.State == ConnectionState.Closed)
                connection.Open();
            return new DbContext();
        }


        /// <summary>
        /// Executes a simple query.
        /// </summary>
        /// <typeparam name="T">Model class</typeparam>
        /// <param name="query">OracleQuery</param>
        /// <returns>IEnumerable with the requested value.</returns>
        public IEnumerable<T> Query<T>(OracleQuery query) {
            return MicroKevin.SqlMapper.Query<T>(connection, query.GetQuery());
        }


        /// <summary>
        /// Executes a simple query.
        /// </summary>
        /// <param name="query">OracleQuery</param>
        /// <returns>Object with the requested value.</returns>
        public object Query(OracleQuery query) {
            return MicroKevin.SqlMapper.Query(connection, query.GetQuery());
        }

        /// <summary>
        /// Executes a simple query, object oriented
        /// </summary>
        /// <param name="obj">Model class. Either empty or with filled properties.</param>
        /// <returns>IEnumerable with the requested value.</returns>
        public IEnumerable<T> Query<T>(IModel obj)
        {
            OracleQuery query = new OracleQuery(OracleQuery.SELECT);
            query.Select("");

            ModelAttribute tableAttr = (ModelAttribute)obj.GetType()
                    .GetCustomAttribute(typeof(ModelAttribute));

            query.From((tableAttr.withQuotes ? "\"" + tableAttr.tableName + "\"" : tableAttr.tableName));

            object valueUsedInForeach;
            foreach (PropertyInfo prop in obj.GetType().GetProperties().Where(x => x.GetCustomAttribute(typeof(IgnoreColumnAttribute)) == null))
            {
                valueUsedInForeach = prop.GetValue(obj, null);
                if (valueUsedInForeach != null && !valueUsedInForeach.Equals(""))
                {
                    query.Where(prop.Name, "=", valueUsedInForeach);
                }
            }
            return this.Query<T>(query);
        }


        /// <summary>
        /// Executes a query. Doesn't get anything in return.
        /// </summary>
        /// <param name="statement">Sql prepared statement</param>
        /// <param name="param">Parameters in an anonymous class</param>
        public void Execute(OracleQuery statement, object param = null)
        {
            MicroKevin.SqlMapper.Execute(connection, statement.GetQuery(), param);
        }


        /// <summary>
        /// Executes a query.
        /// </summary>
        /// <typeparam name="T">Return object type</typeparam>
        /// <param name="query">Query in string</param>
        /// <param name="param">Parameters in an anonymous object</param>
        /// <returns>Dynamic object</returns>
        public T ExecuteScalar<T>(string query, object param = null)
        {
            return MicroKevin.SqlMapper.ExecuteScalar<T>(connection, query, param);
        }


        /// <summary>
        /// Create an insert query.
        /// </summary>
        /// <param name="obj">Model class</param>
        public void Insert(object obj)
        {
            object valueUsedInForeach;

            List<object> values = new List<object>();
            List<string> columns = new List<string>();

            foreach (PropertyInfo prop in obj.GetType().GetProperties().Where(x => x.GetCustomAttribute(typeof(IgnoreColumnAttribute)) == null))
            {
                valueUsedInForeach = prop.GetValue(obj, null);
                if (valueUsedInForeach != null && !valueUsedInForeach.Equals(""))
                {
                    columns.Add(prop.Name);
                    values.Add(valueUsedInForeach);
                }

            }

            OracleQuery query = new OracleQuery(OracleQuery.INSERT);
            ModelAttribute tableAttr = (ModelAttribute)obj.GetType()
                    .GetCustomAttribute(typeof(ModelAttribute));

            query.Insert((tableAttr.withQuotes ? "\"" + tableAttr.tableName + "\"" : tableAttr.tableName),
                values.ToArray(),
                columns.ToArray());

            this.Execute(query);
        }


        /// <summary>
        /// Create a update query.
        /// </summary>
        /// <param name="obj">Model class</param>
        /// <param name="whereObjs">Dictionary for the where clause</param>
        public void Update(object obj, Dictionary<string, object> whereObjs)
        {
            object valueUsedInForeach;

            List<object> values = new List<object>();
            List<string> columns = new List<string>();

            foreach (PropertyInfo prop in obj.GetType().GetProperties().Where(x => x.GetCustomAttribute(typeof(IgnoreColumnAttribute)) == null))
            {
                valueUsedInForeach = prop.GetValue(obj, null);
                if (valueUsedInForeach != null && !valueUsedInForeach.Equals(""))
                {
                    columns.Add(prop.Name);
                    values.Add(valueUsedInForeach);
                }

            }

            OracleQuery query = new OracleQuery(OracleQuery.UPDATE);
            ModelAttribute tableAttr = (ModelAttribute)obj.GetType()
                    .GetCustomAttribute(typeof(ModelAttribute));

            query.Update((tableAttr.withQuotes ? "\"" + tableAttr.tableName + "\"" : tableAttr.tableName),
                values.ToArray(),
                columns.ToArray());

            foreach (KeyValuePair<string, object> whereObj in whereObjs)
                query.Where(whereObj.Key, "=", whereObj.Value);

            this.Execute(query);
        }


        /// <summary>
        /// Create a delete query
        /// </summary>
        /// <param name="obj">Model class</param>
        public void Delete(object obj)
        {
            OracleQuery query = new OracleQuery(OracleQuery.DELETE);
            ModelAttribute tableAttr = (ModelAttribute)obj.GetType()
                    .GetCustomAttribute(typeof(ModelAttribute));
            query.Delete();
            query.From((tableAttr.withQuotes ? "\"" + tableAttr.tableName + "\"" : tableAttr.tableName));

            object valueUsedInForeach;

            foreach (PropertyInfo prop in obj.GetType().GetProperties().Where(x => x.GetCustomAttribute(typeof(IgnoreColumnAttribute)) == null))
            {
                valueUsedInForeach = prop.GetValue(obj, null);
                if (valueUsedInForeach != null && !valueUsedInForeach.Equals(""))
                {
                    query.Where(prop.Name, "=", valueUsedInForeach);
                }

            }

            this.Execute(query);
        }


        /// <summary>
        /// Disposing method. Closing the connection.
        /// </summary>
        public void Dispose()
        {
            connection.Close();
        }
    }
}
