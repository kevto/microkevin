﻿using MicroKevin.Framework.Data;
using MicroKevin.Framework.Data.Models;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using System.ComponentModel;
using System.Diagnostics;

namespace MicroKevin.Framework
{
    /// <summary>
    /// Context class of the framework
    /// </summary>
    public class DbContextOne : IDisposable
    {
        private static OracleConnection connection = null;

        /// <summary>
        /// Public constructor
        /// </summary>
        private DbContextOne()
        {
        }

        public static DbContextOne GetInstance()
        {
            if (connection == null)
                connection = DbConnection.GetOpenConnection();
            else if (connection.State == ConnectionState.Closed)
                connection.Open();
            return new DbContextOne();
        }


        /// <summary>
        /// Executes a simple query.
        /// </summary>
        /// <typeparam name="T">Model class</typeparam>
        /// <param name="query">OracleQuery</param>
        /// <returns>IEnumerable with the requested value.</returns>
        public IEnumerable<T> Query<T>(OracleQuery query) where T : class, new() 
        {
            OracleCommand cmd = new OracleCommand(query.GetQuery(), connection);
            OracleDataReader reader = cmd.ExecuteReader();

            PropertyDescriptorCollection props =
                TypeDescriptor.GetProperties(typeof(T));

            PropertyDescriptor[] propArray = new PropertyDescriptor[reader.FieldCount];
            for (int i = 0; i < propArray.Length; i++)
            {
                propArray[i] = props[reader.GetName(i).ToLower()];
            }
            while (reader.Read())
            {
                T item = new T();

                for (int i = 0; i < propArray.Length; i++)
                {
                    object value = reader.IsDBNull(i) ? null : reader[i];
                    propArray[i].SetValue(item, value);
                }
                yield return item;
            }
        }


        /// <summary>
        /// Executes a simple query, object oriented
        /// </summary>
        /// <param name="obj">Model class. Either empty or with filled properties.</param>
        /// <returns>IEnumerable with the requested value.</returns>
        public IEnumerable<T> Query<T>(IModel obj) where T : class, new() 
        {
            OracleQuery query = new OracleQuery(OracleQuery.SELECT);
            query.Select("");

            ModelAttribute tableAttr = (ModelAttribute)obj.GetType()
                    .GetCustomAttribute(typeof(ModelAttribute));

            query.From((tableAttr.withQuotes ? "\"" + tableAttr.tableName + "\"" : tableAttr.tableName));

            object valueUsedInForeach;
            foreach (PropertyInfo prop in obj.GetType().GetProperties().Where(x => x.GetCustomAttribute(typeof(IgnoreColumnAttribute)) == null))
            {
                valueUsedInForeach = prop.GetValue(obj, null);
                if (valueUsedInForeach != null && !valueUsedInForeach.Equals(""))
                {
                    query.Where(prop.Name, "=", valueUsedInForeach);
                }
            }
            return this.Query<T>(query);
        }


        /// <summary>
        /// Executes a query. Doesn't get anything in return.
        /// </summary>
        /// <param name="statement">Sql prepared statement</param>
        public void Execute(OracleQuery statement)
        {
            OracleCommand cmd = new OracleCommand(statement.GetQuery(), connection);
            cmd.ExecuteNonQuery();
        }


        /// <summary>
        /// Executes a query.
        /// </summary>
        /// <typeparam name="T">Return object type</typeparam>
        /// <param name="query">Query in string</param>
        /// <returns>Dynamic object</returns>
        public T ExecuteScalar<T>(string query)
        {
            OracleCommand cmd = new OracleCommand(query, connection);
            return (T) cmd.ExecuteScalar();
        }


        /// <summary>
        /// Create an insert query.
        /// </summary>
        /// <param name="obj">Model class</param>
        public void Insert(object obj)
        {
            object valueUsedInForeach;

            List<object> values = new List<object>();
            List<string> columns = new List<string>();

            foreach (PropertyInfo prop in obj.GetType().GetProperties().Where(x => x.GetCustomAttribute(typeof(IgnoreColumnAttribute)) == null))
            {
                valueUsedInForeach = prop.GetValue(obj, null);
                if (valueUsedInForeach != null && !valueUsedInForeach.Equals(""))
                {
                    columns.Add(prop.Name);
                    values.Add(valueUsedInForeach);
                }

            }

            OracleQuery query = new OracleQuery(OracleQuery.INSERT);
            ModelAttribute tableAttr = (ModelAttribute)obj.GetType()
                    .GetCustomAttribute(typeof(ModelAttribute));

            query.Insert((tableAttr.withQuotes ? "\"" + tableAttr.tableName + "\"" : tableAttr.tableName),
                values.ToArray(),
                columns.ToArray());

            this.Execute(query);
        }


        /// <summary>
        /// Create a update query.
        /// </summary>
        /// <param name="obj">Model class</param>
        /// <param name="whereObjs">Dictionary for the where clause</param>
        public void Update(object obj, Dictionary<string, object> whereObjs)
        {
            object valueUsedInForeach;

            List<object> values = new List<object>();
            List<string> columns = new List<string>();

            foreach (PropertyInfo prop in obj.GetType().GetProperties().Where(x => x.GetCustomAttribute(typeof(IgnoreColumnAttribute)) == null))
            {
                valueUsedInForeach = prop.GetValue(obj, null);
                if (valueUsedInForeach != null && !valueUsedInForeach.Equals(""))
                {
                    columns.Add(prop.Name);
                    values.Add(valueUsedInForeach);
                }

            }

            OracleQuery query = new OracleQuery(OracleQuery.UPDATE);
            ModelAttribute tableAttr = (ModelAttribute)obj.GetType()
                    .GetCustomAttribute(typeof(ModelAttribute));

            query.Update((tableAttr.withQuotes ? "\"" + tableAttr.tableName + "\"" : tableAttr.tableName),
                values.ToArray(),
                columns.ToArray());

            foreach (KeyValuePair<string, object> whereObj in whereObjs)
                query.Where(whereObj.Key, "=", whereObj.Value);

            this.Execute(query);
        }


        /// <summary>
        /// Create a delete query
        /// </summary>
        /// <param name="obj">Model class</param>
        public void Delete(object obj)
        {
            OracleQuery query = new OracleQuery(OracleQuery.DELETE);
            ModelAttribute tableAttr = (ModelAttribute)obj.GetType()
                    .GetCustomAttribute(typeof(ModelAttribute));
            query.Delete();
            query.From((tableAttr.withQuotes ? "\"" + tableAttr.tableName + "\"" : tableAttr.tableName));

            object valueUsedInForeach;

            foreach (PropertyInfo prop in obj.GetType().GetProperties().Where(x => x.GetCustomAttribute(typeof(IgnoreColumnAttribute)) == null))
            {
                valueUsedInForeach = prop.GetValue(obj, null);
                if (valueUsedInForeach != null && !valueUsedInForeach.Equals(""))
                {
                    query.Where(prop.Name, "=", valueUsedInForeach);
                }

            }

            this.Execute(query);
        }


        /// <summary>
        /// Disposing method. Closing the connection.
        /// </summary>
        public void Dispose()
        {
            connection.Close();
        }
    }
}
