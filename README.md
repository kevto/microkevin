# MicroKevin #

MicroKevin is a C# mini framework build for several school assignments. The reason why MicroKevin is invented, because we were not allowed to use any other framework nor ORM libraries. Therefore we created our own to basically simplify life. We used to use dapper for some of our features, but it didn't seem like a clever idea to use anything but drivers and plain C#.

MicroKevin uses two NuGet packages from Oracle to communicate with a local/remote database. MicroKevin is basically a tiny version of Dapper for Oracle. Keep in mind that we have no unit tests due lack of experience by the time of inventing this.

##Usage##

Simply add a new connection string named "OracleDbContext". MicroKevin only uses that particular connection string. Everything else is done automatically. To start working with MicroKevin, get a new instance of DbContext.


```
#!c#

using(var ctx = DbContext.GetInstance()) 
{
    // Do whatever you want with the context.

}
```

It opens and closes the connection automatically.